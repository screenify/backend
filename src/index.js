import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import config from './config';
import logger from './logger';
import bcryptHelper from './utils/bcryptUtil';
import globals from './utils/globals';
import scheduler from './utils/scheduler';
import DBManager from './db/dbManager';
import RouteManager from './routes/routeManager';
import AppConfig from './db/models/AppConfig';

globals.serverStartupTime = new Date();

const dbManager = new DBManager(
  config.database.host,
  config.database.port,
  config.database.credentials.username,
  config.database.credentials.password,
  config.database.credentials.authDB,
  config.database.database
);

const SERVER = express();

async function startup() {
  logger.info("System startup ...");

  // process.on('SIGINT', shutdown);
  // process.on('SIGTERM', shutdown);
  process.on('uncaughtException', () => shutdown());

  let success = await dbManager.connect();

  if (!success)
   return;

  logger.debug("Loading app configuration from database ...");
  let result = await AppConfig.find({ identifier: "ACTIVE_CONFIG" });
  if (!result || result.length === 0) {
    logger.debug("Missing app configuration. Creating default ...");
    globals.appConfig = new AppConfig();
    await globals.appConfig.save();
  }

  globals.appConfig = result[0];
  logger.debug("Loaded app configuration from database!");

  logger.debug("Starting scheduler ...");
  scheduler.start();
  logger.debug("Scheduler started ...");

  logger.silly("Registering middleware ...");
  SERVER.use(cookieParser());
  SERVER.use(bodyParser.urlencoded({ extended: true }));
  SERVER.use(bodyParser.json());

  let routeManager = new RouteManager(SERVER);
  await routeManager.initialize();
  routeManager.registerAllRoutes();

  SERVER.listen(config.webserver.port, config.webserver.host, function() {
    logger.info(`Webserver is listening on ${config.webserver.host}:${config.webserver.port}`);
    globals.serverStatus.webserver = true;
  });

}

function shutdown(exitCode) {
  if (globals.shutdown) {
    logger.debug("Shutdown called twice!");
    return;
  }

  globals.shutdown = true;
  logger.info("System shutdown ...");
  logger.debug("Stopping webserver ...")
  SERVER.unbind(() => {
    globals.serverStatus.webserver = false;
    logger.info("Webserver closed!");

    dbManager.disconnect(() => {
      process.exit(exitCode || process.exitCode);
    });
  });

}

startup().catch((err) => {
  logger.error("Caught error during startup! Shutting down!");
  console.error(err);
  shutdown(1);
});
