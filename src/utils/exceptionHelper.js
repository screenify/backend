import logger from '../logger';
import DBManager from '../db/dbManager';

export default {
  databaseCatch: (error, logger) => {
      DBManager.catch(error, logger);
  },

  routeCatch: (error, logger) => {
    logger.error("An unexpected error occurred while creating a new user!");
    logger.error(error);
  }
}
