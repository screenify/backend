const { validationResult } = require('express-validator/check');
export default function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    res.end();
    return;
  }

  next();
}
