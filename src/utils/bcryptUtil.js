import bcrypt from 'bcrypt';
import config from '../config';

export async function hash(data, callback) {
  try {
    let hash = await bcrypt.hash(data, config.bcrypt.rounds);
    if (callback)
      callback(null, hash);
    else
      return hash;
  } catch (e) {
    if (callback)
      callback(e);
    else
      throw e;
  }
}

export async function compare(data, hash, callback) {
  try {
    let match = bcrypt.compare(data, hash);
    if (callback)
      callback(null, match);
    else
      return match;
  } catch (e) {
    if (callback)
      callback(e);
    else
      throw e;
  }
}

export default {
    hash: module.exports.hash,
    compare: module.exports.compare
}
