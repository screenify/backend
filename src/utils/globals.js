let globals = {
  serverStartupTime: null,
  maintenance: false,
  serverStatus: {
    webserver: false,
    database: false
  },
  sessions: {
    secretKey: null
  },

  isMaintenance: () => {
    return globals.maintenance || Object.values(globals.serverStatus).some(val => !val);
  },
};

export default globals;
