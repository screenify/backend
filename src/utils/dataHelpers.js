import UserModel from '../db/models/User';
import bcrypt from 'bcrypt';

let functions = {

  fetchUserAdminRestricted: async (req, res, next) => {
    if (!req.session || !req.session.user) {
      throw new Error("fetchUserAdminRestricted only works with a valid session and fetched session user data!");
    }

    if (req.body.uid != req.session.uid && !req.session.user.admin) {
      res.simple(403, "No permission!");
      res.end();
      return;
    }

    let user = req.session.user;
    if (req.body.uid != user.id) {
      let result = await UserModel.find({_id: req.body.uid});

      if (!result || result.length < 1)
        user = null;
      else
        user = result[0];
    }

    if (!user) {
      res.simple(404, "User not found");
      res.end()
      return;
    }

    req.fetched.user = user;
    next();
  },

  confirmationPassword: async (req, res, next) => {
    if (!req.session || !req.session.user) {
      throw new Error("confirmationPassword only works with a valid session and fetched session user data!");
    }

    let passwordValid = await bcrypt.compare(req.body.confirm_password, req.session.user.password);

    if (!passwordValid) {
      res.simple(998, "Confirmation password invalid!");
      res.end();
      return;
    }
  }
};

export default functions;
