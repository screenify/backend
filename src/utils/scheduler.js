import cron from 'node-cron';

import UserModel from '../db/models/User';

const logger = require("../logger").default.getLogger("Scheduler");

export default {
  start: () => {
    //User Deletion
    cron.schedule("0 */10 * * * *", async () => {
      logger.debug("Checking for users to delete ...");
      let date = Date.now();
      let result = await UserModel.find().where('delete').gt(0).lte(date);

      if (!result || result.length === 0) {
        logger.debug("No users to delete!");
        return;
      }

      logger.debug(`Found ${result.length} user(s) to delete!`);

      let stats = {
        users: 0
      }

      for (const user of result) {
        logger.silly(`Deleting user ${user.id}`);
        await user.remove();
        stats.users++;
        //TODO: Remove all user related data (images, videos, etc.)
        logger.silly(`Deleted user ${user.id}`);
      }

      logger.debug(`Deletion finished. Deleted ${stats.users} users!`);
    });
  }
}
