import UserModel from '../db/models/User';

const logger = require('../logger').default.getLogger("AccessFilter");

let functions = {
  userSession: (req, res, next) => {
    if (req.session) {
      next();
      return;
    }

    res.status(403)
    res.end();
  },
  userSessionFetchUser: (req, res, next) => {
    functions.userSession(req, res, async function() {
      let result = await UserModel.find({_id: req.session.uid});

      if (!result || result < 1) {
        logger.debug("UID of valid session not found in database. Clearing cookie!");
        res.session.destroy(res);
        res.status(403);
        res.end();
        return;
      }

      req.session.user = result[0];
      next();
    });
  },
  adminSession: async (req, res, next) => {
    functions.userSessionFetchUser(req, res, function() {
      if (!req.session.user.admin) {
        res.status(403);
        res.end();
        return;
      }

      next();
    });
  }
}

export default functions;
