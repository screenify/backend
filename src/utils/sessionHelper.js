import path from 'path';
import fs from 'fs';
import forge from 'node-forge';
import JWT from 'jsonwebtoken';
import config from '../config';
import cryptoHelper from './cryptoHelper';
import globals from './globals';

const logger = require('../logger').default.getLogger("SessionHelper");

class SessionHelper {

  constructor(secretKeyPath, keyModulusLength) {
    this._secretKeyPath = secretKeyPath;
    this._keyModulusLength = keyModulusLength ? keyModulusLength : 4096;
    this._secretKey = null;
  }

  async initialize() {
    logger.debug("Loading secret key ...");
    if (!fs.existsSync(this._secretKeyPath)) {
      logger.silly("Secret key not found!");
      logger.debug("Generating secret key ...");
      logger.silly("Secret key is generated using RSA algorithm with a modulus length of " + this._keyModulusLength);
      try {
        let keypair = await cryptoHelper.generateKeyPair("rsa", this._keyModulusLength);
        this._secretKey = keypair.privateKey;
        this._publicKey = keypair.publicKey;
        logger.silly("Keypair generated!");

        let dirname = path.dirname(this._secretKeyPath);
        if (!fs.existsSync(dirname))
          fs.mkdirSync(dirname, {
            recursive: true
          });
        fs.writeFileSync(this._secretKeyPath, keypair.privateKey, "utf8");

        logger.silly("Private key saved to " + this._secretKeyPath);
      } catch (e) {
        logger.error("An error occurred during keypair generation process!");
        logger.error(e);

        if (!this._secretKey || !this._publicKey)
          throw new Error("Fatal error during Key generation!");
      }
    } else {
      this._secretKey = fs.readFileSync(this._secretKeyPath);
      let forgePrivkey = forge.pki.privateKeyFromPem(this._secretKey);
      let forgePubkey = forge.pki.setRsaPublicKey(forgePrivkey.n, forgePrivkey.e);
      this._publicKey = forge.pki.publicKeyToPem(forgePubkey);
    }

    logger.debug("Secret key loaded!");
  }

  fetchSession(req, res, next) {
    let cookies = req.cookies;

    if (!(config.webserver.sessions.cookie.cookieKey in cookies)) {
      next();
      return;
    }

    let sessionCookie = cookies[config.webserver.sessions.cookie.cookieKey];

    JWT.verify(sessionCookie, this._publicKey, (err, decoded) => {
      if (err) {
        logger.silly("Invalid session cookie. Requesting removal!");
        res.clearCookie(config.webserver.sessions.cookie.cookieKey);
      } else {
        req.session = {
          uid: decoded.uid,
          destroy: (rsp) => rsp.clearCookie(config.webserver.sessions.cookie.cookieKey)
        };
      }

      next();
    });
  }

  async createSession(uid, callback) {
    let token;
    try {
       token = JWT.sign({
        uid: uid
      }, this._secretKey, {
        expiresIn: config.webserver.sessions.tokenExpiration,
        algorithm: 'RS256'
      });
    } catch (e) {
      if (callback) {
        callback(e);
        return;
      }
      throw e;
    }

    if (callback) {
      callback(null, token);
    }

    return token;
  }

}

export default SessionHelper;
