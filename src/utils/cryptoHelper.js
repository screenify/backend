import crypto from 'crypto';

export default {
  generateKeyPair: async function(algorithm, modulusLength, callback) {
    return new Promise((resolve, reject) => {
      crypto.generateKeyPair(algorithm, {
        modulusLength: modulusLength,
        publicKeyEncoding: {
          type: 'spki',
          format: 'pem'
        },
        privateKeyEncoding: {
          type: 'pkcs8',
          format: 'pem'
        }
      }, (error, publicKey, privateKey) => {
        if (error) {
          if (!callback) {
            reject(error);
          } else {
            callback(error);
            resolve();
          }
          return;
        }

        if (!callback) {
            resolve({publicKey: publicKey, privateKey: privateKey});
        } else {
            callback(null, publicKey, privateKey);
            resolve();
        }
      });
    });
  }
};
