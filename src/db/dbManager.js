import mongoose from 'mongoose';
import globals from '../utils/globals';

const logger = require('../logger').default.getLogger("DBManager");

class DBManager {

  constructor(host, port, username, password, authdb, database) {
    logger.silly(`DBManager construct: ${username}:***@${host}:${port}/${database}?authSource=${authdb}`);
    this._host = host;
    this._port = port;
    this._username = username;
    this._password = password;
    this._authdb = authdb;
    this._database = database;
  }

  async connect() {
    logger.debug("Connecting to database server ...");
    logger.debug("Using Mongoose version " + mongoose.version);
    try {
      mongoose.set('useNewUrlParser', true);
      mongoose.set('useFindAndModify', false);
      mongoose.set('useCreateIndex', true);
      await mongoose.connect(`mongodb://${this._username}:${this._password}@${this._host}:${this._port}/${this._database}?authSource=${this._authdb}`, {useNewUrlParser: true});
    } catch (e) {
      logger.error("Error while connecting to database server!")
      logger.error(e);
      globals.serverStatus.database = false;
      return false;
    }

    this._connection = mongoose.connection;

    this._connection.on('error', (error) => {
      logger.error("Uncaught Error during database processing!");
      logger.error(error);
    });

    this._connection.on('disconnected', () => {
      logger.log( {
        level: this._disconnecting ? "info" : "warn",
        message: "Disconnected from database server!"
      })
      globals.serverStatus.database = false;
    });

    this._connection.on('reconnected', () => {
      logger.info("Reconnected to database server!");
      globals.serverStatus.database = true;
    });

    logger.info(`Connected to ${this._host}:${this._port}/${this._database}`);
    globals.serverStatus.database = true;
    return true;
  }

  disconnect(callback) {
    logger.debug("Disconnecting from database ...");
    this._disconnecting = true;
    return mongoose.connection.close(callback);
  }

  static catch(error, lgger) {
    if (!lgger)
      lgger = logger;
    lgger.error("An uncaught exception during a database operation was caught!");
    lgger.error(error);
  }

}
export default DBManager;
