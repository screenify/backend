import mongoose from 'mongoose';
import { Schema } from 'mongoose';

export default mongoose.model("User", new Schema({
  email: String,
  password: String,
  firstname: String,
  lastname: String,
  admin: Boolean,
  delete: { type: Number, default: -1 }
}));
