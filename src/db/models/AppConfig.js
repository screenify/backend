import mongoose from 'mongoose';
import { Schema } from 'mongoose';

export default mongoose.model("AppConfig", new Schema({
  identifier: { type: String, default: "ACTIVE_CONFIG", index: {unique: true, dropDups: true}},
  userSettings: {
    deletionGracePeriod: { type: Number, default: 1000 * 60 * 60 * 24 },
  }
}, { collection: 'app_config' }));
