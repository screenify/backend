import logger from '../logger';
import config from '../config';
import globals from '../utils/globals';
import SessionHelper from '../utils/sessionHelper';
import exceptionHelper from '../utils/exceptionHelper';

import UserModel from '../db/models/User';

class RouteManager {

  constructor(server) {
    this._server = server;
    this._routes = [];
    this._logger = logger.getLogger("RouteManager");
    this._sessionHelper = new SessionHelper(config.webserver.sessions.secretKey.path, config.webserver.sessions.secretKey.modulusLength);
  }

  _loadRoutes() {
      this._routes.push(require('./public/status.js'));
      this._routes.push(require('./users/newUser.js'));
      this._routes.push(require('./public/login.js'));
      this._routes.push(require('./users/logout.js'));
      this._routes.push(require('./users/getInfo.js'));
      this._routes.push(require('./users/findUsers.js'));
      this._routes.push(require('./users/updateUser.js'));
      this._routes.push(require('./users/deleteUser.js'));
      this._routes.push(require('./users/undeleteUser.js'));
      this._logger.debug(`Loaded ${this._routes.length} routes.`);
  }

  _registerPreHandlers() {
    this._server.use([
      (req, res, next) => {
        res.status(204);

        res.simple = function(code, message) {
          res.status(code);
          res.json({
            status: code,
            message: message
          });
        }

        next();
      },
      (req, res, next) => this._sessionHelper.fetchSession(req, res, next),
      async (req, res, next) => {
        if (!globals.isMaintenance()) {
          next();
          return;
        }

        if (req.session && Object.values(globals.serverStatus).every(val => val)) {
          let result = await UserModel.find({_id: req.session.uid});
          if (result && result.length > 0) {
            let user = result[0];
            if (user.admin) {
              next();
              return;
            }
          }
        }

        res.status(503);
        res.simple(503, "Maintenance");
        res.end();
      }
    ]);
  }

  async initialize() {
    this._loadRoutes();
    this._registerPreHandlers();
    await this._sessionHelper.initialize();
  }

  registerAllRoutes() {
    for (let n in this._routes) {
      let route = this._routes[n];
      let routeLogger = logger.getLogger(route.NAME);
      route.register(this._server, routeLogger, config, globals, this._sessionHelper, exceptionHelper);
      this._logger.silly(`Registered route: ${route.NAME}`);
    }
  }

  get routes() {
    return this._routes;
  }

}

export default RouteManager;
