import accessFilter from '../../utils/accessFilter';
import autoError from '../../utils/autoError';
import { check } from 'express-validator/check';

import UserModel from '../../db/models/User';

export const NAME = "UserGetInfo";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.get(
    "/users/getinfo",
    [
      check("uid").optional().isMongoId()
    ],
    autoError,
    accessFilter.userSessionFetchUser,
    async (req, res, next) => {
      let user = req.session.user;
      if (req.query.uid) {
        let result = await UserModel.find({_id: req.query.uid});

        if (!result || result.length < 1)
          user = null;
        else
          user = result[0];
      }

      if (!user) {
        res.simple(404, "User not found");
        next();
        return;
      }

      let out = {
        id: user.id,
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        admin: user.admin
      };

      if (req.session.user != user && !req.session.user.admin) {
        delete out.email;
        out.lastname = out.lastname.substring(0, 1).toUpperCase();
      }

      res.status(200);
      res.json(out);
    }
  );
};
