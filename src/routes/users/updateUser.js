import accessFilter from '../../utils/accessFilter';
import autoError from '../../utils/autoError';
import { check } from 'express-validator/check';
import bcrypt from 'bcrypt';

import UserModel from '../../db/models/User';

export const NAME = "UpdateUser";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.post(
    "/users/update",
    [
      check("uid").exists().isMongoId(),
      check("confirm_password").exists().isLength({ min: 8 }), //Confirmation password to allow change
      check("password").optional().isLength({ min: 8 }), //Potential new user password
      check("email").optional().isEmail(),
      check("firstname").optional().isLength({ min:1 }).isAlpha(),
      check("lastname").optional().isLength({ min:1 }).isAlpha()
    ],
    autoError,
    accessFilter.userSessionFetchUser,
    async (req, res, next) => {
      if (req.body.uid != req.session.uid && !req.session.user.admin) {
        res.simple(403, "No permission!");
        next();
        return;
      }

      let user = req.session.user;
      if (req.body.uid != user.id) {
        let result = await UserModel.find({_id: req.body.uid});

        if (!result || result.length < 1)
          user = null;
        else
          user = result[0];
      }

      if (!user) {
        res.simple(404, "User not found");
        next();
        return;
      }

      let passwordValid = await bcrypt.compare(req.body.confirm_password, req.session.user.password);

      if (!passwordValid) {
        res.simple(900, "Confirmation password invalid!");
        next();
        return;
      }

      let out = {
        updated: []
      }

      if (req.body.password) {
        let hash = await bcrypt.hash(req.body.password, config.bcrypt.rounds);
        user.password = hash;
        out.updated.push("password");
      }

      if (req.body.email) {
        user.email = req.body.email;
        out.updated.push("email");
      }

      if (req.body.firstname) {
        user.firstname = req.body.firstname;
        out.updated.push("firstname");
      }

      if (req.body.lastname) {
        user.lastname = req.body.lastname;
        out.updated.push("lastname");
      }

      await user.save();

      res.status(200);
      res.json(out);
    }
  );
};
