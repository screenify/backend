import accessFilter from '../../utils/accessFilter';

export const NAME = "Logout";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.get(
    "/users/logout",
    accessFilter.userSession,
    async (req, res, next) => {
      req.session.destroy(res);
      res.simple(200, "Successfully logged out!");
    }
  );
};
