import accessFilter from '../../utils/accessFilter';
import autoError from '../../utils/autoError';
import { check } from 'express-validator/check';
import bcrypt from 'bcrypt';

import UserModel from '../../db/models/User';

export const NAME = "DeleteUser";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.post(
    "/users/delete",
    [
      check("uid").exists().isMongoId(),
      check("confirm_password").exists().isLength({ min: 8 }), //Confirmation password to allow change
    ],
    autoError,
    accessFilter.userSessionFetchUser,
    async (req, res, next) => {
      if (req.body.uid != req.session.uid && !req.session.user.admin) {
        res.simple(403, "No permission!");
        next();
        return;
      }

      let user = req.session.user;
      if (req.body.uid != user.id) {
        let result = await UserModel.find({_id: req.body.uid});

        if (!result || result.length < 1)
          user = null;
        else
          user = result[0];
      }

      if (!user) {
        res.simple(404, "User not found");
        next();
        return;
      }

      let passwordValid = await bcrypt.compare(req.body.confirm_password, req.session.user.password);

      if (!passwordValid) {
        res.simple(900, "Confirmation password invalid!");
        next();
        return;
      }

      user.delete = Date.now() + globals.appConfig.userSettings.deletionGracePeriod;

      await user.save();

      res.status(200);
      res.json({
        timeOfDeletion: user.delete
      });
      next();
    }
  );
};
