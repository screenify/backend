import accessFilter from '../../utils/accessFilter';
import autoError from '../../utils/autoError';
import dataHelpers from '../../utils/dataHelpers';
import { check } from 'express-validator/check';
import bcrypt from 'bcrypt';

import UserModel from '../../db/models/User';

export const NAME = "UndeleteUser";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.post(
    "/users/undelete",
    [
      check("uid").exists().isMongoId(),
      check("confirm_password").exists().isLength({ min: 8 }), //Confirmation password to allow change
    ],
    autoError,
    accessFilter.userSessionFetchUser,
    dataHelpers.confirmationPassword,
    dataHelpers.fetchUserAdminRestricted,
    async (req, res, next) => {
      let user = req.fetched.user;

      if (user.delete <= -1) {
        req.simple(900, "User not scheduled for deletion!");
        next();
        return;
      }

      user.delete =-1;

      await user.save();

      res.status(200, "Deletion cancelled!");
      next();
    }
  );
};
