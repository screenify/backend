import accessFilter from '../../utils/accessFilter';
import autoError from '../../utils/autoError';
import { check } from 'express-validator/check';

import UserModel from '../../db/models/User';

export const NAME = "UserFind";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.post(
    "/users/find",
    [
      check("uid").optional().isMongoId()
    ],
    autoError,
    accessFilter.adminSession,
    async (req, res, next) => {
      let user = req.session.user;
      let results = [];
      if (req.query.uid) {
        results = await UserModel.find({_id: req.query.uid});
      } else {
        results = await UserModel.find({});
      }

      res.status(200);
      res.json(results);
    }
  );
};
