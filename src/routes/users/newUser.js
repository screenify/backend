import autoError from '../../utils/autoError';
import {
  check
} from 'express-validator/check';
import bcrypt from 'bcrypt';

import UserModel from '../../db/models/User';

export const NAME = "NewUser";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.post(
    "/users/new",
    [
      check("email").exists().isEmail(),
      check("password").exists().isLength({
        min: 8
      }),
      check("firstname").optional().isLength({ min:1 }).isAlpha(),
      check("lastname").optional().isLength({ min:1 }).isAlpha()
    ],
    autoError,
    async (req, res, next) => {
      try {
        let count = await UserModel.countDocuments({
          email: req.body.email.toLowerCase()
        });
        if (count > 0) {
          res.simple(900, "E-Mail already exists.");
          return;
        }

        let hash = await bcrypt.hash(req.body.password, config.bcrypt.rounds);

        let user = new UserModel({
          email: req.body.email,
          password: hash,
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          admin: false
        });

        await user.save();
        res.simple(200, "Success");
      } catch (error) {
        exceptionHelper.routeCatch(logger, e);
        res.status(500);
        res.status(500);
      } finally {
        next();
      }
    }
  );
};
