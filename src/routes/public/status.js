export const NAME = "StatusRoute";
export function register(server, loggger, config, globals, sessionHelper) {
  server.get("/status", (req, res, next) => {
    let out = {
      maintenance: globals.isMaintenance(),
      detail: {
        webserver: globals.serverStatus.webserver,
        database: globals.serverStatus.database
      }
    };

    res.status(200);
    res.json(out);
    next();
  });
};
