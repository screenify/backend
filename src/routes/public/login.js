import autoError from '../../utils/autoError';
import {
  check
} from 'express-validator/check';
import bcrypt from 'bcrypt';

import UserModel from '../../db/models/User';

export const NAME = "Login";
export function register(server, logger, config, globals, sessionHelper, exceptionHelper) {
  server.post(
    "/login",
    [
      check("email").exists().isEmail(),
      check("password").exists().isLength({ min: 8 })
    ],
    autoError,
    async (req, res, next) => {
      try {
        if (req.session) {
          res.simple(999, "Unavailable while logged in");
          return;
        }

        let results = await UserModel.find({
          email: req.body.email.toLowerCase()
        });

        if (!results || results.length < 1) {
          res.simple(900, "E-Mail or password invalid!");
          return;
        }

        let user = results[0];
        let passValid = await bcrypt.compare(req.body.password, user.password);

        if (!passValid) {
          res.simple(900, "E-Mail or password invalid!");
          return;
        }

        let token = await sessionHelper.createSession(user.id);

        res.cookie(config.webserver.sessions.cookie.cookieKey, token, {
          expires: new Date(Date.now() + config.webserver.sessions.cookie.maxAge),
          httpOnly: config.webserver.sessions.cookie.httpOnly,
          secure: config.webserver.sessions.cookie.secure
        });
        res.simple(200, "Successfully logged in!");
      } catch (e) {
        exceptionHelper.routeCatch(e, logger);
        res.status(500);
      } finally {
        next();
      }
    }
  );
};
