import {
  createLogger,
  format,
  transports
} from 'winston';
import dailyRotate from 'winston-daily-rotate-file';
import clc from 'cli-color';
import config from '../config';
import path from 'path';
import ChildManager from './childManager';

const colors = {
  error: 'red',
  debug: 'blue',
  warn: 'yellow',
  data: 'grey',
  info: 'green',
  verbose: 'cyan',
  silly: 'magenta',
  custom: 'yellow'
};

const combinedJsonLog = format.combine(format.timestamp(), format.json());

const prettyLog = format.combine(
  format.timestamp({
    format: "HH:mm:ss"
  }),
  format.printf(info => {
    let colorizedLevel = clc.bold(clc[colors[info.level]](info.level.toUpperCase()));
    let loggerName = info.loggerName ? info.loggerName : "Server"
    return `[${info.timestamp}][${loggerName}][${colorizedLevel}] ` + clc['blackBright']("» ") + info.message;
  })
);

const logger = createLogger({
  level: 'info',
  format: combinedJsonLog
});

const CHILD_MANAGER = new ChildManager(logger);

if (config.logger.consoleSettings.enabled) {
  logger.add(new transports.Console({
    level: config.logger.consoleSettings.level,
    format: prettyLog,
    handleExceptions: true
  }));
}

if (config.logger.logfileSettings.enabled) {
  let fileSettings = config.logger.logfileSettings;
  logger.add(new transports.DailyRotateFile({
    format: fileSettings.json ? combinedJsonLog : format.combine(format.timestamp({
      format: 'YY-MM-DD HH:mm:ss'
    }), format.printf(info => `[${info.timestamp}][${info.level.toUpperCase()}] ${info.message}`)),
    level: fileSettings.level,
    dirname: fileSettings.directory,
    filename: fileSettings.filename,
    datePattern: fileSettings.datePattern,
    zippedArchive: fileSettings.zippedArchive,
    maxSize: fileSettings.maxSize,
    maxFiles: fileSettings.maxFiles,
  }));
}

logger.getLogger = (name) => CHILD_MANAGER.getChild(name);

export default logger;
