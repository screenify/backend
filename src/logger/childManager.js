import stackTrace from 'stack-trace';
import path from 'path';

class ChildManager {

  constructor(parent) {
    this.parent = parent;
    this.childLoggers = {};
  }

  getChild(name) {
    if (!name) {
      let trace = stackTrace.get();
      let frame = trace[1];
      let filename = frame.getFileName()
      name = path.basename(filename).split('.');
      name.pop();
      name = name.join('.')
    }

    if (name.toLowerCase() === "server")
      return this.parent;

    if (name in this.childLoggers) {
      return this.childLoggers[name];
    }

    let child = this.parent.child({loggerName: name});
    this.childLoggers[name] = child;
    return child;
  }
}

export default ChildManager;
