module.exports = {
  filesystem: {
    imageStorage: "./storage/images",
    videoStorage: "./storage/videos",
    audioStorage: "./storage/audio"
  },
  database: {
    host: "127.0.0.1",
    port: 27017,
    credentials: {
      username: "default",
      password: "supersecret!123",
      authDB: "admin"
    },
    database: "screenify"
  },
  webserver: {
    host: "0.0.0.0",
    port: 8080,
    sessions: {
      secretKey: {
        path: "./keys/session.key",
        modulusLength: 8192
      },
      tokenExpiration: '1d',
      cookie: {
        cookieKey: 'sfy_session',
        maxAge: 1000 * 60 * 60 * 24, // 24h
        httpOnly: false,
        secure: false
      }
    },
    recaptcha: {
      privateKey: ""
    }
  },
  bcrypt: {
    rounds: 10
  },
  logger: {
    consoleSettings: {
      enabled: false,
      json: false,
      level: 'info'
    },
    logfileSettings: {
      enabled: true,
      json: true,
      level: 'info',
      directory: 'logs',
      filename: '%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zipedArchive: true,
      maxSize: '20m',
      maxFiles: null
    }
  }
}
