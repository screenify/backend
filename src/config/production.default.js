/*
  Default production configuration. Rename to "production.js" to activate
 */
module.exports = {
  database: {
    host: "127.0.0.1",
    port: 27017,
    credentials: {
      username: "default",
      password: "supersecret!123"
    }
  },
  webserver: {
    host: "0.0.0.0",
    port: 80
  }
}
