module.exports = {
  webserver: {
    host: process.env.BIND,
    port: process.env.PORT
  }
}
