import merge from 'lodash.merge';
import defaults from './default.js';
import envConfig from './env.js';
var config = require('./' + (process.env.NODE_ENV || "development") + ".js");

export default merge({}, defaults, config, envConfig);
