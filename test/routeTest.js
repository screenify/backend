import assert from 'assert';
import RouteManager from '../src/routes/routeManager';

let routeManager = new RouteManager();
let routes = routeManager.routes;

for (let n in routes) {
  let route = routeManager.routes[n];
  describe(`Route ${n} (${route.NAME})`, () => {
    it('should have name', done => {
      assert(typeof route.NAME !== "undefined", "Route has no name defined!");
      done();
    });
    it('should have register function', done => {
      assert(typeof route.register === "function", "Route has no register function!");
      done();
    });
  });
}
